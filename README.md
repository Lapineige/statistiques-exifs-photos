**[Version française]**

# Ce que fais ce script

Ce script python affiche quelques statistiques sur une collection de photos, à partir des métadonnées exifs.

Actuellement seule la focale (équivalente 35mm) est affichée : une liste de (focale+nombre de photos) est renvoyée.

Fonctionne sur les RAW (format RW2, facilement modificiable dans le script pour traiter d'autres formats de RAW ou des JPEG).

# Comment l'installer
Prérequis : avoir installé le programme `fd` https://github.com/sharkdp/fd (utilisé pour la recherche, et beaucoup plus rapide que `find`)
- Télécharger le script
- Le lancer via la commande `python3 statistiques_exifs.py`

---
---
---

**[English version]**

# What does this script do ?

This python script displays some statistics about a collection of pictures, using their exif metadata.

For the moment only the focal lenght (35mm equivalent) is displayed : a list of (focal+number of pictures) is showed.

Process RAW files (RW2 format, easily editable in the script to process other RAW formats or JPEG)

# How to install
Prerequisite : you need to install `fd` https://github.com/sharkdp/fd (used for the research, and way faster than `find`)
- Download the script
- Run it via this command `python3 statistiques_exifs.py`
