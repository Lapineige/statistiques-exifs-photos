import subprocess
from pprint import pprint
from os.path import basename

### recursive search for files
# by default, it searches recursivly in current directory
# if you want to specify one or several folder to inspect, add "path1, path2" at the end of "fdfind RW2 -e RW2"
# Default file format is RW2, panasonic raw format. To change the file format, replace both "RW2" by your extension (CR2, JPEG, …)
# The search will ignore any file that does not end with .RW2 extension (for instance .RW2.xmp files are ignored)
file_list = bytes.decode(subprocess.check_output("fdfind RW2 -e RW2", shell=True)).split('\n')[:-1]

stats_dict = dict()
already_done = list()

for file in file_list:
    if basename(file) in already_done: #avoid doubles
        continue
    already_done.append(basename(file))

    print("Progression: " , file_list.index(file), '/', len(file_list))

    key = int(bytes.decode(
                        subprocess.check_output("""exiftool -FocalLengthIn35mmFormat "{}" """.format(file), shell=True)
                        ).split(': ')[-1].split(" mm")[0])
    stats_dict[key] = stats_dict.get(key, 0) + 1

print("Results: ")
#pprint(stats_dict)
for key, value in sorted(stats_dict.items(), key=lambda x: x[0]):
    print("{} : {}".format(key, value))

print("Number of unique pictures: ", len(already_done))
